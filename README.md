## Ausführung

- Rust und der Packetmanager Cargo müssen auf dem Rechner installiert sein. (Siehe https://doc.rust-lang.org/book/ch01-01-installation.html)
  - Cargo wird normalerweise bereits mit Rust installiert.
- Mit `cargo run` werden die entsprechenden Depdencies runtergeladen und das Programm wird ausgeführt.
- Mit `cargo build --release` kann eine optimierte Version erstellt werden, welche im Verzeichnis /target/release abgelegt wird.
